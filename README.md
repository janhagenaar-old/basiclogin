An app made in a couple of hours to freshen up my node.js skill.

I used node.js with express as webserver and mongodb as datastorage.


Install procedure:
- Clone repostitory to disk
- Run npm-install
- Add new db: mongod --dbpath c:\path\to\repostitory
- Run the app: node app 
- Enjoy ;-)