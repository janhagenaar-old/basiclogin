﻿var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res) {
    res.send('respond with a resource');
});


/* GET register page. */
router.get('/register', function (req, res) {
    res.render('register', { title: 'Registration' });
});

// POST register form
router.post('/register', function (req, res) {
    
    var username = req.body.username;
    var password = req.body.password;
    var email = req.body.email;
    
    var db = req.db
    
    var collection = db.get('usercollection');
    
    collection.insert({ username: username, password: password, email: email}, {}, function (e, docs) {
        res.send('User with the following credentials has been added <hr />Username = ' + username + '<hr /> Password = ' + password + '<hr /> Email = ' + email);
    });
});


/* GET Userlist page. */
router.get('/userlist', function (req, res) {
    var db = req.db;

    var collection = db.get('usercollection');

    collection.find({}, {}, function (e, docs) {
        res.render('userList', {
            "userlist" : docs
        });
    });
});

module.exports = router;