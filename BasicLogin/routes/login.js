﻿var express = require('express');
var router = express.Router();

/* GET login page. */
router.get('/', function (req, res) {
    res.render('login', { title: 'BasicLogin' });
});

// POST login form
router.post('/', function (req, res) {
    
    var username = req.body.username;
    var password = req.body.password;
    var email = req.body.email;

    res.send('Username = ' + username + '<hr /> Password = ' + password + '<hr /> Email = ' + email);
});

module.exports = router;